import React from 'react';
import { StyleSheet, View, Image, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { images } from '../../constants';
import { useState } from 'react/cjs/react.development';

const ListItemQA = (props) => {

    const [openAnswer, setOpenAnswer] = useState(false);
    const { questionText, answerText } = props;

    function _opneAnswer() {
        if(openAnswer === false) {
            setOpenAnswer(true);
        }else {
            setOpenAnswer(false);
        }
    }

    return (
        <View style={styles.container}>
            <View style={{ ...styles.flex_row, ...styles.container_question }}>
                <View style={{ ...styles.flex_row }}>
                    <Image source={images.question_i} />
                    <View style={{ width: 10, }} />
                    <Text>{questionText}</Text>
                </View>
                <TouchableOpacity onPress={_opneAnswer}>
                    <Image source={openAnswer ? images.arrow_up : images.arrow_down} />
                </TouchableOpacity>
            </View>
            <View style={{height:10}} />
            {openAnswer ?
                <View style={{ ...styles.container_answer, ...styles.flex_row }}>
                    <Image source={images.answer_i} />
                    <View style={{ width: 10, }} />
                    <Text style={{flexShrink:1}}>{answerText}</Text>
                </View> : <View />
            }
            <View style={{height: 0.8, backgroundColor:'#A4A4A4'}} />
        </View>
    );
}

ListItemQA.propTypes = {
    questionText: PropTypes.string,
    answerText: PropTypes.string
}

export default ListItemQA;

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'column',
        marginBottom: 10
    },
    flex_row: {
        display: 'flex',
        flexDirection: 'row',
    },
    container_question: {
        justifyContent: 'space-between'
    },
    container_answer: {
        backgroundColor: '#F9F9F9',
        paddingVertical: 20,
        paddingHorizontal: 10,
        marginTop: 10,
    }
});