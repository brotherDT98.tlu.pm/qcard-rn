import React from 'react';
import { View, StyleSheet, Text, FlatList } from 'react-native';
import ListItemQA from './ListItemQA';
import PropTypes from 'prop-types';
import { useState } from 'react/cjs/react.development';
import { useEffect } from 'react';


const ListComponentQA = (props) => {

    const { dataQAs } = props;
    return (
        <View style={styles.container}>
            {/* <FlatList
                data={dataQAs}
                renderItem={({ item }) => <Text>{item.question}</Text>}
                keyExtractor={(item) => item.question}
            /> */}
            <Text>{dataQAs.length}</Text>
        </View>
    );
}

ListComponentQA.propTypes = {
    dataQAs: PropTypes.array
}

export default ListComponentQA;

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});