import React from 'react';
import { View, ScrollView, SafeAreaView, TouchableOpacity, StyleSheet, Image, Text } from 'react-native';
import { SIZES } from '../../constants';
import PropTypes from 'prop-types';

const Scaffold = (props) => {
    
    const { children, title, leading, actions } = props
    return (

        <SafeAreaView style={styles.container}>
            <View style={styles.app_bar}>
                {leading}
                <Text style={styles.titile_style}>{title}</Text>
                {actions}
            </View>
            <ScrollView>
                <View style={styles.child_style}>
                    {children}
                </View>
            </ScrollView>
        </SafeAreaView>


    );
}

Scaffold.propTypes = {
    children: PropTypes.element.isRequired,
    title: PropTypes.string,
    leading: PropTypes.element.isRequired,
    actions: PropTypes.element
}

export default Scaffold;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    app_bar: {
        width: SIZES.width,
        height: 90,
        backgroundColor: '#FF9F33',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    child_style: {
        padding: 20,
    },
    titile_style: {
        color: 'white',
        fontSize: 20
    },
    flex_row: {
        display: 'flex',
        flexDirection: 'row'
    }
});