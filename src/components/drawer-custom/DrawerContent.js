import React from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';
import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import { Avatar, Title, Caption, Paragraph, Drawer, TouchableRipple, Switch } from 'react-native-paper';
import { SIZES, images } from '../../constants';

export function DrawerContent(props) {

    return (
        <View style={styles.drawerContent}>
            <View style={styles.userInfoSection}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Avatar.Image source={images.avatar} size={70} />
                    <View style={{ marginLeft: 15, flexDirection: 'row' }}>
                        <TouchableOpacity>
                            <Image source={images.noti_i} />
                        </TouchableOpacity>
                        <View style={{ width: 20, }} />
                        <TouchableOpacity>
                            <Image source={images.setting_i} />
                        </TouchableOpacity>
                        <View style={{ width: 20, }} />
                    </View>
                </View>
                <View style={{ paddingVertical: 10 }}>
                    <Text style={styles.caption}>Anh Suke</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.paragraph}>bcdefg123@naver.com</Text>
                    <Text style={styles.paragraph}>로그아웃</Text>
                </View>
            </View>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>
                    <Drawer.Section style={styles.drawerSection}>
                        <TouchableRipple onPress={() => console.log('click item drawer !')}>
                            <View style={styles.preference}>
                                <Text>큐카드뉴스 홈</Text>
                                <Image source={images.arrow_down} />
                            </View>
                        </TouchableRipple>
                    </Drawer.Section>
                    <Drawer.Section style={styles.drawerSection}>
                        <TouchableRipple onPress={() => console.log('click item drawer !')}>
                            <View style={styles.preference}>
                                <Text>이용 가이드</Text>
                                <Image source={images.arrow_down} />
                            </View>
                        </TouchableRipple>
                    </Drawer.Section>
                    <Drawer.Section style={styles.drawerSection}>
                        <TouchableRipple onPress={() => console.log('click item drawer !')}>
                            <View style={styles.preference}>
                                <Text>자주하는 질문</Text>
                                <Image source={images.arrow_down} />
                            </View>
                        </TouchableRipple>
                    </Drawer.Section>
                    <Drawer.Section style={styles.drawerSection}>
                        <TouchableRipple onPress={() => console.log('click item drawer !')}>
                            <View style={styles.preference}>
                                <Text>공지사항</Text>
                                <Image source={images.arrow_down} />
                            </View>
                        </TouchableRipple>
                    </Drawer.Section>
                    <Drawer.Section style={styles.drawerSection}>
                        <TouchableRipple onPress={() => console.log('click item drawer !')}>
                            <View style={styles.preference}>
                                <Text>문의하기</Text>
                                <Image source={images.arrow_down} />
                            </View>
                        </TouchableRipple>
                    </Drawer.Section>
                    <Drawer.Section style={styles.drawerSection}>
                        <TouchableRipple onPress={() => console.log('click item drawer !')}>
                            <View style={styles.preference}>
                                <Text>건의사항</Text>
                                <Image source={images.arrow_down} />
                            </View>
                        </TouchableRipple>
                    </Drawer.Section>
                    <View style={styles.ads}>
                        <Text style={{ fontSize: 28, color: '#999999' }}>280*233</Text>
                    </View>
                </View>
            </DrawerContentScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
    },
    userInfoSection: {
        paddingLeft: 20,
        height: SIZES.height * 0.25,
        backgroundColor: '#2C2C2C',
        flexDirection: 'column',
        justifyContent: 'center'
    },
    title: {
        fontSize: 16,
        marginTop: 3,
        fontWeight: 'bold'
    },
    caption: {
        fontSize: 16,
        color: 'white'
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    paragraph: {
        color: '#AFAFAF',
        marginRight: 20
    },
    drawerSection: {
        marginTop: 15
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    preference: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16
    },
    ads: {
        marginHorizontal: 20,
        marginVertical: 40,
        height: 233,
        backgroundColor: '#EEEEEE',
        alignItems: 'center',
        justifyContent: 'center'
    }
});