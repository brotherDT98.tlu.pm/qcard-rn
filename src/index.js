import React from 'react';
import Root from './App';
import RootNavigation from './navigation/RootNavigation';

// USING CLASS COMPONENT AS A WORKAROUND FOR HOT RELOADING
export default class App extends React.Component {
  render() {
    return <Root />;
  }
}