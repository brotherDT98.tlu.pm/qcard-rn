const ads = require('../assets/images/ads.png');
const add_text = require('../assets/images/add.png');
const answer_i = require('../assets/images/answer.png');
const apple_i = require('../assets/images/apple.png');
const arrow_down = require('../assets/images/arrow_dow.png');
const arrow_up = require('../assets/images/arrow_up.png');
const avatar = require('../assets/images/avatar.png');
const back_i = require('../assets/images/back_i.png');
const bot_left_i = require('../assets/images/bot_left.png');
const bot_right_i = require('../assets/images/bot_right.png');
const check_i =  require('../assets/images/check_i.png');
const del_i =  require('../assets/images/del.png');
const arrow_down_text =  require('../assets/images/dow.png');
const dowload_i =  require('../assets/images/dowload.png');
const facebook_i =  require('../assets/images/facebook.png');
const google_i =  require('../assets/images/google.png');
const kakao_i =  require('../assets/images/kakao.png');
const logo =  require('../assets/images/logo.png');
const menu_i =  require('../assets/images/menu_i.png');
const naver_i =  require('../assets/images/naver.png');
const noti_i =  require('../assets/images/noti_i.png');
const pass_i =  require('../assets/images/pass_i.png');
const question_i =  require('../assets/images/question.png');
const roate_i =  require('../assets/images/roate.png');
const setting_i =  require('../assets/images/setting.png');
const top_left_i =  require('../assets/images/top_left.png');
const top_right_i =  require('../assets/images/top_right.png');
const un_check_i = require('../assets/images/uncheck_i.png');
const up_i_text = require('../assets/images/up.png');
const user_i = require('../assets/images/user_i.png');
export default {
    ads,
    add_text,
    answer_i,
    apple_i,
    arrow_down,
    arrow_up,
    avatar,
    back_i,
    bot_left_i,
    bot_right_i,
    check_i,
    del_i,
    arrow_down_text,
    dowload_i,
    facebook_i,
    google_i,
    kakao_i,
    logo,
    menu_i,
    naver_i,
    noti_i,
    pass_i,
    question_i,
    roate_i,
    setting_i,
    top_left_i,
    top_right_i,
    un_check_i,
    up_i_text,
    user_i
}