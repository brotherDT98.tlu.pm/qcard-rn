import React from 'react';
import { SafeAreaView, TouchableOpacity, Image, FlatList, StyleSheet, Text, View } from 'react-native';
import ListItemQA from '../../components/listQA/ListItemQA';
import Scaffold from '../../components/scaffold/Scaffold';
import { images } from '../../constants';

const dataQA = [
    {
        question: 'What is Lorem Ipsum?',
        answer: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    },
    {
        question: 'Why do we use it?',
        answer: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)."
    },
    {
        question: 'Where can I get some?',
        answer: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc."
    }
]


const QAScreen = ({navigation}) => {
    
    function _onPressBack() {
        console.log('back !')
        navigation.goBack();
    }

    function _onPressMenu() {
        navigation.openDrawer();
    }

    function renderLeading() {
        return (
            <TouchableOpacity onPress={_onPressBack}>
                <Image source={images.back_i} />
            </TouchableOpacity>
        );
    }

    function renderActions() {
        return (
            <View style={{ display: 'flex', flexDirection: 'row' }}>
                <TouchableOpacity >
                    <Image source={images.dowload_i} />
                </TouchableOpacity>
                <View style={{ width: 10, }} />
                <TouchableOpacity style={{ marginTop: 3 }} onPress={_onPressMenu}>
                    <Image source={images.menu_i} />
                </TouchableOpacity>
                <View style={{ width: 10, }} />
            </View>
        );
    }

    function renderChildren() {
        return (
            <FlatList
                scrollEnabled={false}
                data={dataQA}
                renderItem={({ item }) => <ListItemQA questionText={item.question} answerText={item.answer} />}
                keyExtractor={(item) => item.question}
            />
        );
    }

    return (
        <Scaffold
            leading={renderLeading()}
            actions={renderActions()}
            title='문의하기'
            children={renderChildren()}
        />
    );
}
export default QAScreen;

const styles = StyleSheet.create({
    container: {
        padding: 20
    }
});
