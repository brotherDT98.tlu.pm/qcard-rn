import React from 'react';
import {
    TouchableOpacity,
    View,
    SafeAreaView,
    StyleSheet,
    Text,
    Image,
    ScrollView
} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { useState } from 'react/cjs/react.development';
import { SIZES, images } from '../../constants';
import { LoginManager, AccessToken } from "react-native-fbsdk";
import { GoogleSignin, statusCodes } from 'react-native-login-google';
import { useEffect } from 'react';

const LoginScreen = ({navigation}) => {

    const [value, onChangeText] = React.useState('');
    const [tokenFacebook, setTokenFacebook] = useState('');

    useEffect(() => {
        GoogleSignin.configure();
    });

    function _onPress() { 
        navigation.navigate("QAScreen");
    }

    function _loginWithFacebook() {
        LoginManager.logInWithPermissions(["public_profile"]).then(
            function (result) {
                if (result.isCancelled) {
                    console.log("Login cancelled");
                }
                return AccessToken.getCurrentAccessToken();
            },
            function (error) {
                console.log("Login fail with error: " + error);
            }
        ).then((data) => setTokenFacebook(data.userID.toString()));
    }

    async function _loginWithGoogle() {
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            setTokenFacebook(userInfo.user.id.toString())
          } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
              console.log('user cancelled the login flow');
            } else if (error.code === statusCodes.IN_PROGRESS) {
              console.log('operation (f.e. sign in) is in progress already');
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
              console.log('play services not available or outdated');
            } else {
              console.log('some other error happened : ', error.toString());
            }
          }
    }

    async function _logOutAllMethod() {
        try {
            // await GoogleSignin.revokeAccess(); // check google is loggined ?
            await GoogleSignin.signOut(); //logout google
            LoginManager.logOut(); //logout facebook
            setTokenFacebook('');
          } catch (error) {
            console.error(error);
          }
    }

    return (
        <ScrollView>
            <SafeAreaView style={styles.container}>
                <View style={styles.container_child}>
                    <View>
                        <Image source={images.logo} />
                    </View>
                    <View style={{ height: 20 }} />
                    <View style={styles.text_input_container}>
                        <Image style={styles.icon_input} source={images.user_i} />
                        <TextInput
                            style={styles.text_input}
                            placeholder="이메일"
                            onChangeText={text => onChangeText(text)}
                            value={value}
                        />
                    </View>

                    <View style={styles.text_input_container}>
                        <Image style={styles.icon_input} source={images.pass_i} />
                        <TextInput
                            style={styles.text_input}
                            placeholder="비밀번호"
                            onChangeText={text => onChangeText(text)}
                            value={value}
                        />
                    </View>
                    <View style={{ height: 9, }} />
                    <View style={styles.checkbox_container}>
                        <View style={styles.flex_row}>
                            <Image source={images.check_i} />
                            <View style={{ width: 4 }} />
                            <Text style={styles.text_content}>이메일 저장</Text>
                        </View>
                        <View style={{ width: 30 }} />
                        <View style={styles.flex_row}>
                            <Image source={images.un_check_i} />
                            <View style={{ width: 4 }} />
                            <Text style={styles.text_content}>이메일 저장</Text>
                        </View>
                    </View>
                    <View style={{ height: 22 }} />
                    <TouchableOpacity style={styles.button_submit} onPress={() => _onPress()}>
                        <Text style={{ color: '#FFFFFF' }}>로그인</Text>
                    </TouchableOpacity>
                    <View style={{ height: 9 }} />
                    <View style={styles.flex_row}>
                        <Text style={styles.text_content}>회원가입 </Text>
                        <View style={{
                            width: 10,
                            borderRightWidth: 1,
                            marginRight: 10,
                            borderColor: '#666666'
                        }} />
                        <Text style={styles.text_content}>비밀번호 찾기</Text>
                    </View>
                    <View style={{ height: 43 }} />
                    <Text style={styles.text_content}>SNS 계정 간편 로그인</Text>
                    <View style={{ height: 19 }} />
                    <View style={styles.flex_row}>
                        <TouchableOpacity>
                            <Image style={styles.icon_social} source={images.naver_i} />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Image style={styles.icon_social} source={images.kakao_i} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={_loginWithFacebook}>
                            <Image style={styles.icon_social} source={images.facebook_i} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={_loginWithGoogle}>
                            <Image style={styles.icon_social} source={images.google_i} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={_logOutAllMethod}>
                            <Image style={styles.icon_social} source={images.apple_i} />
                        </TouchableOpacity>
                    </View>
                    <View>
                        <Text>{tokenFacebook}</Text>
                    </View>
                </View>
                <View style={styles.ads_container}>
                    <Text style={{
                        fontSize: 24,
                        color: '#999999'
                    }}>300*50</Text>
                </View>
            </SafeAreaView>
        </ScrollView>
    );
}

export default LoginScreen;

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        // flex: 1,
        height: SIZES.height,
        backgroundColor: 'white'
    },
    container_child: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    text_input_container: {
        display: 'flex',
        flexDirection: 'row',
        height: 50,
        borderColor: '#DDDDDD',
        borderStyle: 'solid',
        borderWidth: 1,
        width: SIZES.width * 0.9,
        marginTop: 10,
        alignItems: 'center'
    },
    icon_input: {
        marginRight: 11.45,
        marginLeft: 16
    },
    text_input: {
        flex: 1
    },
    checkbox_container: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        width: SIZES.width * 0.9,
    },
    flex_row: {
        display: 'flex',
        flexDirection: 'row'
    },
    text_content: {
        color: '#666666'
    },
    button_submit: {
        backgroundColor: '#FF9F33',
        height: 50,
        borderColor: '#DDDDDD',
        borderStyle: 'solid',
        borderWidth: 1,
        width: SIZES.width * 0.9,
        alignItems: 'center',
        justifyContent: 'center'
    },
    ads_container: {
        position: 'absolute',
        bottom: 0,
        height: 62.5,
        backgroundColor: '#EEEEEE',
        width: SIZES.width,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon_social: {
        margin: 5
    }
});