import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import {LoginScreen, QAScreen } from '../screens';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { DrawerContent } from '../components/drawer-custom/DrawerContent';
import { SIZES } from '../constants';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const RootNavigation = () => {
  return (
    <NavigationContainer>
      {/* <Stack.Navigator
        screenOptions={{
          headerShown: false
        }}
        initialRouteName={"LoginScreen"}
      > 
        <Stack.Screen name='LoginScreen' component={LoginScreen} />
        <Stack.Screen name="HomeScreen" component={HomeScreen} />
        <Stack.Screen name="EditScreen" component={EditScreen} />
        <Stack.Screen name="QAScreen" component={QAScreen} />
      </Stack.Navigator> */}
      <Drawer.Navigator drawerStyle={{
        width: SIZES.width * 0.85,
      }} initialRouteName="LoginScreen" drawerContent={props => <DrawerContent {...props} />}>
        <Drawer.Screen options={{ swipeEnabled: false }} name='LoginScreen' component={LoginScreen} />
        <Drawer.Screen name='QAScreen' component={QAScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

export default RootNavigation;

